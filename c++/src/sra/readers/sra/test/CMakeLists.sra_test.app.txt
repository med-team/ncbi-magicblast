#############################################################################
# $Id: CMakeLists.sra_test.app.txt 567016 2018-07-11 17:34:16Z dicuccio $
#############################################################################

NCBI_begin_app(sra_test)
  NCBI_sources(sra_test)
  NCBI_uses_toolkit_libraries(ncbi_xloader_genbank sraread)
  NCBI_project_watchers(vasilche ucko)
  NCBI_add_include_directories(${SRA_INCLUDE})
NCBI_end_app()

if(OFF)
#
#
#
add_executable(sra_test-app
    sra_test
)

set_target_properties(sra_test-app PROPERTIES OUTPUT_NAME sra_test)

include_directories(SYSTEM ${SRA_INCLUDE})

target_link_libraries(sra_test-app
    ncbi_xloader_genbank sraread
)
endif()
