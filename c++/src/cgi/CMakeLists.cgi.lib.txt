#############################################################################
# $Id: CMakeLists.cgi.lib.txt 564553 2018-05-30 14:04:12Z gouriano $
#############################################################################

NCBI_begin_lib(xcgi)
  NCBI_sources(
    ncbicgi cgiapp cgictx ncbicgir ncbires ref_args cgi_run cgi_util
    cgi_serial cgi_session cgi_exception cgiapp_cached cgi_entry_reader
    user_agent
  )
  NCBI_uses_toolkit_libraries(xutil)
  NCBI_project_watchers(vakatov)
NCBI_end_lib()

if(OFF)
#
#
#
add_library(xcgi
    ncbicgi cgiapp cgictx ncbicgir ncbires ref_args cgi_run cgi_util
    cgi_serial cgi_session cgi_exception cgiapp_cached cgi_entry_reader
    user_agent
)
include_directories(SYSTEM ${FASTCGI_INCLUDE})

target_link_libraries(xcgi
    xutil
)
endif()
