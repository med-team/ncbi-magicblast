#############################################################################
# $Id: CMakeLists.medlars.asn.txt 564454 2018-05-29 13:35:44Z gouriano $
#############################################################################

NCBI_begin_lib(medlars)
  NCBI_dataspecs(medlars.asn)
  NCBI_uses_toolkit_libraries(general biblio)
NCBI_end_lib()


if(OFF)

set(MODULE medlars)
set(MODULE_IMPORT objects/general/general objects/biblio/biblio)
set(MODULE_PATH objects/medlars)

set(MODULE_EXT "asn")
add_library(medlars ${MODULE}__ ${MODULE}___)

RunDatatool("${MODULE}" "${MODULE_IMPORT}")

target_link_libraries(${MODULE}
    pubmed
)

target_link_libraries(medlars
    biblio
)
endif()
