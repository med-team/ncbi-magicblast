#############################################################################
# $Id: CMakeLists.id1.asn.txt 564454 2018-05-29 13:35:44Z gouriano $
#############################################################################

NCBI_begin_lib(id1)
  NCBI_dataspecs(id1.asn)
  NCBI_uses_toolkit_libraries(seqset seq)
  NCBI_project_watchers(vasilche)
NCBI_end_lib()


if(OFF)

set(MODULE id1)
set(MODULE_IMPORT objects/seqloc/seqloc objects/seqset/seqset objects/seq/seq)
set(MODULE_PATH objects/id1)

set(MODULE_EXT "asn")
add_library(id1 ${MODULE}__ ${MODULE}___)

RunDatatool("${MODULE}" "${MODULE_IMPORT}")

target_link_libraries(id1
    seqset
)
endif()
