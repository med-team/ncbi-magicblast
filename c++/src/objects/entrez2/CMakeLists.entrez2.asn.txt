#############################################################################
# $Id: CMakeLists.entrez2.asn.txt 564454 2018-05-29 13:35:44Z gouriano $
#############################################################################

NCBI_begin_lib(entrez2)
  NCBI_dataspecs(entrez2.asn)
  NCBI_uses_toolkit_libraries(xser)
  NCBI_project_watchers(lavr)
NCBI_end_lib()


if(OFF)

set(MODULE entrez2)
set(MODULE_IMPORT )
set(MODULE_PATH objects/entrez2)

set(MODULE_EXT "asn")
add_library(entrez2 ${MODULE}__ ${MODULE}___)

RunDatatool("${MODULE}" "${MODULE_IMPORT}")

target_link_libraries(entrez2
    xser
)
endif()

