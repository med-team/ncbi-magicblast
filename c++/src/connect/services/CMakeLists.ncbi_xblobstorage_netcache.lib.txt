#############################################################################
# $Id: CMakeLists.ncbi_xblobstorage_netcache.lib.txt 567665 2018-07-23 12:58:02Z gouriano $
#############################################################################

NCBI_begin_lib(ncbi_xblobstorage_netcache SHARED)
  NCBI_sources(blob_storage_netcache)
  NCBI_add_definitions(NCBI_BLOBSTORAGE_NETCACHE_EXPORTS)
  NCBI_uses_toolkit_libraries(xconnserv)
  NCBI_project_watchers(sadyrovr)
NCBI_end_lib()


if(OFF)
#
#
#
add_library(ncbi_xblobstorage_netcache
    blob_storage_netcache
)
target_link_libraries(ncbi_xblobstorage_netcache
    xconnserv
)
endif()
