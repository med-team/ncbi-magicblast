#############################################################################
# $Id: CMakeLists.linkhsp_unit_test.app.txt 566834 2018-07-09 12:39:41Z gouriano $
#############################################################################

NCBI_begin_app(linkhsp_unit_test)
  NCBI_sources(linkhsp_unit_test)
  NCBI_uses_toolkit_libraries(blast_unit_test_util xblast)
  NCBI_project_watchers(boratyng madden camacho fongah2)
  NCBI_add_test()
NCBI_end_app()

if(OFF)
#
#
#
add_executable(linkhsp_unit_test-app
    linkhsp_unit_test
)

set_target_properties(linkhsp_unit_test-app PROPERTIES OUTPUT_NAME linkhsp_unit_test)



target_link_libraries(linkhsp_unit_test-app
    blast_unit_test_util xblast
)
endif()
