#############################################################################
# $Id: CMakeLists.ntlookup_unit_test.app.txt 566834 2018-07-09 12:39:41Z gouriano $
#############################################################################

NCBI_begin_app(ntlookup_unit_test)
  NCBI_sources(ntlookup_unit_test)
  NCBI_uses_toolkit_libraries(blast_unit_test_util xblast)
  NCBI_project_watchers(boratyng morgulis madden camacho fongah2)
  NCBI_add_test()
NCBI_end_app()

if(OFF)
#
#
#
add_executable(ntlookup_unit_test-app
    ntlookup_unit_test
)

set_target_properties(ntlookup_unit_test-app PROPERTIES OUTPUT_NAME ntlookup_unit_test)



target_link_libraries(ntlookup_unit_test-app
    blast_unit_test_util xblast
)
endif()
