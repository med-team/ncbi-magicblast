#############################################################################
# $Id: CMakeLists.hspfilter_culling_unit_test.app.txt 566834 2018-07-09 12:39:41Z gouriano $
#############################################################################

NCBI_begin_app(hspfilter_culling_unit_test)
  NCBI_sources(hspfilter_culling_unit_test)
  NCBI_uses_toolkit_libraries(blast)
  NCBI_project_watchers(boratyng madden camacho fongah2)
  NCBI_add_test()
NCBI_end_app()

if(OFF)
#
#
#
add_executable(hspfilter_culling_unit_test-app
    hspfilter_culling_unit_test
)

set_target_properties(hspfilter_culling_unit_test-app PROPERTIES OUTPUT_NAME hspfilter_culling_unit_test)



target_link_libraries(hspfilter_culling_unit_test-app
    blast test_boost
)
endif()
