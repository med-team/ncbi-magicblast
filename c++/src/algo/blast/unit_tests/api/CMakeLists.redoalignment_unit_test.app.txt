#############################################################################
# $Id: CMakeLists.redoalignment_unit_test.app.txt 566834 2018-07-09 12:39:41Z gouriano $
#############################################################################

NCBI_begin_app(redoalignment_unit_test)
  NCBI_sources(redoalignment_unit_test)
  NCBI_add_include_directories(${NCBI_CURRENT_SOURCE_DIR}/../../core)
  NCBI_uses_toolkit_libraries(blast_unit_test_util blastinput)
  NCBI_project_watchers(boratyng madden)
  NCBI_add_test()
NCBI_end_app()

if(OFF)
#
#
#
add_executable(redoalignment_unit_test-app
    redoalignment_unit_test
)

set_target_properties(redoalignment_unit_test-app PROPERTIES OUTPUT_NAME redoalignment_unit_test)



target_link_libraries(redoalignment_unit_test-app
    blast_unit_test_util blastinput
)
endif()
