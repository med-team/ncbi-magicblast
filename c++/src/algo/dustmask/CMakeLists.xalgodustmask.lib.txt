#############################################################################
# $Id: CMakeLists.xalgodustmask.lib.txt 566834 2018-07-09 12:39:41Z gouriano $
#############################################################################

NCBI_begin_lib(xalgodustmask)
  NCBI_sources(symdust)
  NCBI_uses_toolkit_libraries(seqset xobjmgr)
  NCBI_project_watchers(morgulis)
NCBI_end_lib()

if(OFF)
#
#
#
add_library(xalgodustmask
    symdust
)
add_dependencies(xalgodustmask
    seqset
)

target_link_libraries(xalgodustmask
    xobjmgr
)
endif()
