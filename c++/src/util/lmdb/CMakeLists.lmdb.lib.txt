#############################################################################
# $Id: CMakeLists.lmdb.lib.txt 564559 2018-05-30 14:36:48Z dicuccio $
#############################################################################

NCBI_begin_lib(lmdb)
  NCBI_sources(mdb midl)
  NCBI_add_include_directories(${includedir}/util/lmdb)
  NCBI_project_watchers(ivanov)
  NCBI_uses_external_libraries(${CMAKE_THREAD_LIBS_INIT})
NCBI_end_lib()
