#############################################################################
# $Id: CMakeLists.xregexp.lib.txt 568619 2018-08-08 15:57:35Z dicuccio $
#############################################################################
#
#
#
if (ON)
NCBI_begin_lib(xregexp)
  NCBI_sources(regexp arg_regexp mask_regexp)
  NCBI_uses_toolkit_libraries(xncbi)
  NCBI_project_watchers(ivanov)
  if (USE_LOCAL_PCRE)
    NCBI_uses_toolkit_libraries(regexp)
  else()
    NCBI_requires(PCRE)
  endif()
NCBI_end_lib()

else()
add_library(xregexp
    regexp arg_regexp mask_regexp
)
include_directories(SYSTEM ${PCRE_INCLUDE})

target_link_libraries(xregexp
    ${PCRE_LIBS} xncbi
)
endif()
