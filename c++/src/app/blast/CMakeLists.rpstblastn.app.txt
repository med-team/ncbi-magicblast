#
#
#
add_executable(rpstblastn-app
    rpstblastn_app
)

set_target_properties(rpstblastn-app PROPERTIES OUTPUT_NAME rpstblastn)

target_link_libraries(rpstblastn-app
    blast_app_util
)

